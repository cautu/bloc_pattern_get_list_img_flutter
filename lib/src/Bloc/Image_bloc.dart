import 'package:rxdart/rxdart.dart';

import '../models/image_model.dart';
import '../Api/Image_Api.dart';

class ImageBloc {
  final ImageApi api;
  Stream<List<Images>> _images = Stream.empty();

  BehaviorSubject<String> _query = BehaviorSubject<String>();

  Stream<List<Images>> get images => _images;

  Sink<String> get query => _query;

  ImageBloc(this.api) {
    _query.stream.listen((query) {
      if (query.isEmpty) {
        getImagesDefault();
      }
    });
    getImagesDefault();
    _images = _query.distinct().asyncMap(api.makeRequest).asBroadcastStream();
  }

  getImagesDefault() {
    _query.sink.add('dog');
  }

  void dispose() {
    _query.close();
  }
}
