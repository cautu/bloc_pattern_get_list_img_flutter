import 'package:flutter/material.dart';
import 'package:flutter_bloc_pattern_test/src/Api/Image_Api.dart';

import './Provider/Image_Provider.dart';
import "./screens/imagelist_screen.dart";
import './Bloc/Image_bloc.dart';
import './Api/Image_Api.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ImagesProvider(
        imageBloc: ImageBloc(ImageApi()),
        child: MaterialApp(
          title: 'View Images',
          theme: new ThemeData(primarySwatch: Colors.deepOrange),
          home: Scaffold(
            appBar: AppBar(title: Text('View images')),
            body: ImageList(),
          ),
        ));
    ;
  }
}
