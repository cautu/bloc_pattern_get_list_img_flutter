import 'package:flutter/material.dart';
import '../Bloc/Image_bloc.dart';
import '../Api/Image_Api.dart';

class ImagesProvider extends InheritedWidget {
  final ImageBloc imageBloc;

  ImagesProvider({Key key, ImageBloc imageBloc, Widget child})
      : this.imageBloc = imageBloc ?? ImageBloc(ImageApi()),
        super(key: key, child: child);

  static ImageBloc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(ImagesProvider) as ImagesProvider)
          .imageBloc;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return true;
  }
}
