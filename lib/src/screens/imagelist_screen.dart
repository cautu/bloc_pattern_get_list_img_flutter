import 'package:flutter/material.dart';
import '../Bloc/Image_bloc.dart';
import '../Provider/Image_Provider.dart';

class ImageList extends StatefulWidget {
  @override
  ImageListState createState() {
    // TODO: implement createState
    return ImageListState();
  }
}

class ImageListState extends State<ImageList> {
  @override
  Widget build(BuildContext context) {
    final imageBloc = ImagesProvider.of(context);
    // TODO: implement build
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          _searchField(imageBloc),
          _displayImage(imageBloc),
        ],
      ),
    );
  }

  Widget _searchField(ImageBloc _bloc) {
    return TextField(
      onChanged: _bloc.query.add,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          prefixIcon: Icon(Icons.search),
          hintText: 'Search ...'),
    );
  }

  Widget _displayImage(ImageBloc _bloc) {
    return Expanded(
      child: StreamBuilder(
        stream: _bloc.images,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.data.length == 0) {
            return Center(
              child: Text('Empty'),
            );
          }

          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, int index) {
              return _buildImage(snapshot.data[index]);
            },
          );
        },
      ),
    );
  }

  Widget _buildImage(dynamic image) {
    return Container(
      margin: EdgeInsets.all(20.0),
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: Image.network(
              image.url,
              fit: BoxFit.fitWidth,
              height: 200.0,
            ),
          ),
          Text(image.description == null
              ? ' No description'
              : image.description),
        ],
      ),
    );
  }
}
