class Images {
  final String url;
  final String description;

  Images(this.url, this.description);
  
  Images.fromJson(Map<String, dynamic> json)
      : url = json['urls']['small'],
        description = json['description'];

  Map<String, dynamic> toJson() => {url: url, description: description};
}
