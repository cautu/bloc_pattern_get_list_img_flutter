import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
// import 'dart:async';

class BlocController extends BlocBase {
  BlocController();

  var _counterController = BehaviorSubject<int>.seeded(0);

  Stream<int> get outCounter => _counterController.stream;

  Sink<int> get inCouter => _counterController.sink;

  increment() {
    inCouter.add(_counterController.value + 1);
  }

  @override
  void dispose() {
    _counterController.close();
    // TODO: implement dispose
    super.dispose();
  }
}
